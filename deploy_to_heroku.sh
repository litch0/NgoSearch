#!/bin/sh

podman login --username=_ --password=$(heroku auth:token) registry.heroku.com

podman build -t registry.heroku.com/ongsearch/web .

podman push registry.heroku.com/ongsearch/web

heroku container:release web