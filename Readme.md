# OngSearch

A Project to search Ngos. Demo available on [the main site](https://ongsearch.herokuapp.com/)

As heroku does not offer a free tier anymore, this app had to be shutted down. However You can still run it locally if you have Node installed.

In one shell run:
```
cd server && yarn dev
```
In another shell run:
```
cd client && yarn dev
```
Or if you have make installed use:
```
make
```
to automatically run both of them
