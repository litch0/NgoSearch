all:
	${MAKE} run_server & ${MAKE} run_client

run_server: 
	cd server && yarn dev

run_client: 
	cd client && yarn dev

build: clean build_server build_client
	cp server/package.json dist/package.json

	mv client/dist dist/dist

	cd dist && yarn

clean: 
	rm -rf dist

build_server: 
	cd server && \
		yarn && \
		yarn build

build_client: 
	cd client && \
		yarn && \
		yarn build

install:
	cd client && \
		yarn
	
	cd server && \
		yarn