import { Controller, Get, Logger, MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { GraphQLModule } from "@nestjs/graphql"
import { config } from 'dotenv';
import { MongooseModule } from '@nestjs/mongoose';
import { OngModule } from './Ong/Ong.module';
import { AuthModule } from './Auth/Auth.module';
import { AuthController } from './Auth/Auth.controller';
import { AppService } from './App.service';
import { join } from 'path';

if (process.env.PRODUCTION !== "true")
{
  config({
    path: `${process.cwd()}/../.env`,
    debug: true
  })
}
Logger.warn(`Using Mongo Creds ${process.env.MONGO_URL}`)
@Module({
  imports: [
    AuthModule,
    OngModule,
    GraphQLModule.forRoot({
      debug: false,
      playground: true,
      autoSchemaFile: "schema.gql",
      path: "/api/gql",
      context: ({req}) => ({headers: req.headers, cookies: req.cookies})
    }),
    MongooseModule.forRoot(process.env.MONGO_URL),
  ],
})
export class AppModule {}
