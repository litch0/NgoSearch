import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { FilterQuery, Model } from "mongoose";
import { AppService } from "src/App.service";
import { CreateOngArgs, IOng, OngDocument } from "./Ong.model";


@Injectable()
export class OngService {
    constructor(
        @InjectModel('Ong') private db: Model<OngDocument>,
        private appServ: AppService,
    ){}

    public async search(text: string): Promise<OngDocument[]> {
        // Requires a Text Index
        return await this.db.aggregate([
            { $match: { $text: { $search: text } } },
            { $sort: { score: { $meta: "textScore" } } },
        ])
    }

    public async insertOne(ong: CreateOngArgs): Promise<OngDocument> {
        let doc = new this.db({
            ...ong,
            _id: this.appServ.genID(),
        });
        return await doc.save();
    }

    public async findOneAndUpdate(_id: String, ong: CreateOngArgs): Promise<OngDocument> {
        let doc = new this.db({
            ...ong,
            _id,
        });
        
        return await this.db.findOneAndUpdate({_id }, doc);;
    }

    public async findOne(filter: FilterQuery<OngDocument>): Promise<OngDocument>
    {
        return await this.db.findOne(filter);
    }

    public async findAll(start: number, limit: number): Promise<OngDocument[]>
    {
        return await this.db.find({})
            .skip(start)
            .limit(limit);
    }

    // public async createTextIndex() {
    //     this.db.
    // }

    public async findOneById(_id: String): Promise<OngDocument>
    {
        return await this.db.findById(_id);
    }
}