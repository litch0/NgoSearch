import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AppService } from "src/App.service";
import { UserSchema } from "src/Auth/Auth.model";
import { AuthService } from "src/Auth/Auth.service";
import { OngSchema } from "./Ong.model";
import { OngResolver } from "./Ong.resolver";
import { OngService } from "./Ong.service";

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Ong', schema: OngSchema }]),
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]),
    ],
    providers: [
        AppService,
        OngService,
        OngResolver, 
        AuthService,
    ],
})
export class OngModule { }