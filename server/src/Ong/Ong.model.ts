import { Field, ObjectType, InputType } from '@nestjs/graphql';
import { Document, Mongoose, Schema } from 'mongoose';
import { isJsxOpeningElement } from 'typescript';

export interface IOng {
    _id?: String,
    name: String,
    cattegories: String[],
    introduction: String,
    curiosities?: String,
    site?: String,
    creationDate?: String, 
    images?: String[],
    localization?: String,
    howCanYouHelp?: String,
    contact?: String,
}

export const OngSchema = new Schema({
    _id: String,
    name: String,
    cattegories: [String],
    introduction: String,
    curiosities: String,
    site: String,
    creationDate: String, 
    images: [String],
    localization: String,
    howCanYouHelp: String,
    contact: String,
})

export class OngDocument extends Document implements IOng {
    name: String;
    cattegories: String[];
    introduction: String;
    curiosities: String;
    site: String;
    creationDate: String;
    images: String[];
    localization: String;
    howCanYouHelp: String;
    contact: String;
}
@InputType()
export class CreateOngArgs {
    @Field(() => String)
    name: String;

    @Field(() => [String])
    cattegories: String[];

    @Field(() => String)
    introduction: String;
    
    @Field(() => String, {nullable: true})
    curiosities?: String;

    @Field(() => String, {nullable: true})
    site?: String;
    
    @Field(() => String, {nullable: true})
    creationDate?: String;
    
    @Field(() => [String], {nullable: true})
    images?: String[];
    
    @Field(() => String, {nullable: true})
    localization?: String;
    
    @Field(() => String, {nullable: true})
    howCanYouHelp?: String;
    
    @Field(() => String, {nullable: true})
    contact?: String;
}


@ObjectType()
export class OngOType implements IOng {
    @Field(() => String)
    _id: String;

    @Field(() => String)
    name: String;

    @Field(() => [String])
    cattegories: String[];

    @Field(() => String)
    introduction: String;
    
    @Field(() => String, {nullable: true})
    curiosities?: String;

    @Field(() => String, {nullable: true})
    site?: String;
    
    @Field(() => String, {nullable: true})
    creationDate?: String;
    
    @Field(() => [String], {nullable: true})
    images?: String[];
    
    @Field(() => String, {nullable: true})
    localization?: String;
    
    @Field(() => String, {nullable: true})
    howCanYouHelp?: String;
    
    @Field(() => String, {nullable: true})
    contact?: String;
}