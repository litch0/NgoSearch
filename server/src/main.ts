import * as express from "express"
import * as cookieParser from "cookie-parser"
import { mountNest } from "./App.middleware";
import { Logger } from "@nestjs/common";
import { createProxyMiddleware } from "http-proxy-middleware";

function clientServer() {
    Logger.warn(`configuring server proxy for development`)
    return createProxyMiddleware( (path, req) =>  req.method === 'GET', {
      target: "http://localhost:5000/", 
      ws: true
    })
}

const PORT = process.env.PORT || 3000
const PRODUCTION = process.env.PRODUCTION === "true" || false;
async function main() {

  const app = express();
  
  app.use(cookieParser())
  
  // Proxy of the client
  if (!PRODUCTION)
  {
    app.get("/*", (req, res, next) => {
        if (!req.path.includes('/api'))
        {
          return clientServer()(req, res, next);
        }
        else {next()}
    })
  } 
  else {
    Logger.warn("Production Environment detected")
    app.use(express.static('dist'))
    app.get('*', (req, res, next) => {
      if (!req.path.includes("/api"))
      { res.sendFile('dist/__app.html', { root: __dirname }) }
      else { next() }
    });
  }
  
  await mountNest(app);

  await app.listen(PORT, () => Logger.warn(`Server runing on http://localhost:${PORT}`));
}

main()