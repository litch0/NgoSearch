import type { User } from "../models/user";
import { gql } from "@apollo/client/core";
import { writable } from "svelte/store";
import { client } from "./GraphqlService";

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
  }

function deleteCookie( name, path?, domain? ) {
    
    document.cookie = name + "=" +
        ((path) ? ";path="+path:"")+
        ((domain)?";domain="+domain:"") +
        ";expires=Thu, 01 Jan 2000 00:00:01 GMT;";
}

/*
    undefined - authorizing
    true      - authorized
    false     - notAuthorized
*/
const AuthState = writable<boolean>(undefined);


class AuthService {
    
    public user: User = null;
    public accessToken = null;
    private refreshToken = null;
    private already_authenticated = false;

    async authenticate(): Promise<User> {
        if (this.already_authenticated) {
            return this.user;
        }
        this.already_authenticated = true;        
        this.refreshToken = getCookie('refresh_token') || "refresh token not found";
        
        let accessTokenRequest = await client.mutate({mutation: gql` 
            mutation AccessTokenQuery($rt: String!) { 
                createUserAccessToken(refreshToken: $rt)
            }`,
            variables: {
                "rt": this.refreshToken
            } 
        })
        this.accessToken = accessTokenRequest.data.createUserAccessToken;
        
        let userRequest = await client.query({query: gql`
            query userQuery($accessToken: String!) {
                user(accessToken: $accessToken) { _id name email authorization} 
            }`,
            variables: {
                "accessToken": this.accessToken || "didn't get refresh token"
            }
        })
        
        this.user = userRequest.data.user;
        AuthState.set(this.user != null)

        return this.user;
    }

    public authenticated () {
        return this.user != null
    } 

    public async refresh() {
        this.already_authenticated = false
        AuthState.set(undefined)

        await this.authenticate()        
    }

    public async logout() 
    {
        deleteCookie("refresh_token")
        await this.refresh()
    }
} 

const Auth = new AuthService()
export {Auth, AuthState};