export interface User {
    _id: string;
    name: string;
    email: string;
    authorization: number;
    state: string;
}